package com.iut.lannion.graph.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URL;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.iut.lannion.graph.io.GraphReader;
import com.iut.lannion.graph.model.Edge;
import com.iut.lannion.graph.model.Graph;
import com.iut.lannion.graph.model.Vertex;

/**
 * 
 * Test de la lecture d'un graphe au format SHP
 * 
 * @author etassel
 *
 */
public class GraphReaderTest {

	private GraphReader reader;

	@Before
	public void setUp() {
		this.reader = new GraphReader();
	}

	/**
	 * Récupération d'une ressource à partir de src/main/resources
	 * 
	 * @param name
	 * @return
	 */
	private File getResourceFile(String name) {
		File file = new File(name);
		System.out.println(file.getAbsolutePath());
		return file;
	}

	@Test
	public void testRoute500() throws Exception {
		File file = getResourceFile("src/test/resources/route500/idf/troncon_route.shp");
		Assert.assertTrue(file.exists());

		Graph graph = reader.read(file);
		for (Vertex vertex : graph.getVertices()) {
			assertNotNull(vertex.getId());
			assertNotNull(vertex.getCoordinate());
		}

		for (Edge edge : graph.getEdges()) {
			assertNotNull(edge.getId());
			assertNotNull(edge.getSource());
			assertNotNull(edge.getTarget());
			assertTrue(edge.getCost() > 0.0);
		}

		assertEquals(19207, graph.getVertices().size());
		assertEquals(44348, graph.getEdges().size());

		{
			Vertex v = graph.findVertex("1");
			assertNotNull(v);
			assertEquals("1", v.getId());
			assertEquals(3, graph.getOutEdges(v).size());
			assertEquals(3, graph.getInEdges(v).size());
		}
		{
			Vertex v = graph.findVertex("2");
			assertNotNull(v);
			assertEquals("2", v.getId());
			assertEquals(2, graph.getOutEdges(v).size());
			assertEquals(2, graph.getInEdges(v).size());
		}
	}

}
